<?php
namespace Excellence\Customerattr\Block\Attribute;

class Show extends \Excellence\Customerattr\Block\BaseBlock
{
    public function getCustomersAttribute()
    {
        $collection = $this->_collectionFactory->load();
        $attrCollection = $collection->getData();
        return $attrCollection;
    }
}
