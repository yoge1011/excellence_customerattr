<?php
namespace Excellence\Customerattr\Block\Adminhtml;
class Customerattr extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_customerattr';/*block grid.php directory*/
        $this->_blockGroup = 'Excellence_Customerattr';
        $this->_headerText = __('Customerattr');
        $this->_addButtonLabel = __('Add Attribute'); 
        parent::_construct();
		
    }
}
