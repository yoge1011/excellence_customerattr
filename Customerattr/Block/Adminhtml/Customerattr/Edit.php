<?php
namespace Excellence\Customerattr\Block\Adminhtml\Customerattr;

/**
 * CMS block edit form container
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected function _construct()
    {
        $this->_objectid = 'customerattr_id';
        $this->_blockGroup = 'Excellence_Customerattr';
        $this->_controller = 'adminhtml_customerattr';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Attribute'));
        $this->buttonList->update('delete', 'label', __('Delete Attribute'));

        $this->buttonList->add(
            'saveandcontinue',
            array(
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => array(
                    'mage-init' => array('button' => array('event' => 'saveAndContinueEdit', 'target' => '#edit_form')),
                ),
            ),
            -100
        );

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceByid('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'attribute_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'attribute_content');
                }
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('checkmodule_checkmodel')->getid()) {
            return __("Edit Attribute '%1'", $this->escapeHtml($this->_coreRegistry->registry('checkmodule_checkmodel')->getTitle()));
        } else {
            return __('New Attribute');
        }
    }
}   