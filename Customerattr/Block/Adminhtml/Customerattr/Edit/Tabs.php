<?php
namespace Excellence\Customerattr\Block\Adminhtml\Customerattr\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_customerattr_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Customer Attribute Information'));
    }
}