<?php
namespace Excellence\Customerattr\Model;

class Customerattr extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'customerattr';

    protected function _construct()
    {
        $this->_init('Excellence\Customerattr\Model\ResourceModel\Customerattr');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
