<?php
namespace Excellence\Customerattr\Model\ResourceModel\Customerattr;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Excellence\Customerattr\Model\Customerattr', 'Excellence\Customerattr\Model\ResourceModel\Customerattr');
    }
}
