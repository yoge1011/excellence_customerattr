<?php
namespace Excellence\Customerattr\Model\ResourceModel;

class Customerattr extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('customerattr', 'customerattr_id');
    }
}
