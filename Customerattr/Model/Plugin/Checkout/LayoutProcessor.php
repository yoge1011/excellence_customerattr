<?php
namespace Excellence\Customerattr\Model\Plugin\Checkout;

class LayoutProcessor
{
    protected $customerattrfactory;

    public function __construct(
        \Excellence\Customerattr\Model\Customerattr $customerattrfactory
    ) {
        $this->customerattrfactory = $customerattrfactory;
    }
    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array $jsLayout
    ) {
        $customAttribute = $this->customerattrfactory;
        $collection = $customAttribute->getCollection();
        $i = 251;
        foreach ($collection as $row) {

            if ($row->getData('attribute_type') == "Text") {
                $component = 'Magento_Ui/js/form/element/abstract';
                $elementTmpl = 'ui/form/element/input';
                $options = [];
            } else {
                $component = 'Magento_Ui/js/form/element/select';
                $elementTmpl = 'ui/form/element/select';
                $options = [
                    [
                        'value' => '',
                        'label' => 'NO',
                    ],
                    [
                        'value' => '1',
                        'label' => 'YES',
                    ],
                ];
            }
            $customAttributeCode = $row->getData('attribute_code');
            $customField = [

                'component' => $component,

                'config' => [
                    // customScope is used to group elements within a single form (e.g. they can be validated separately)
                    'customScope' => 'shippingAddress.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => $elementTmpl,
                ],
                'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
                'label' => $row->getData('attribute_label'),
                'provider' => 'checkoutProvider',
                'sortOrder' => $i++,
                'validation' => [],
                'options' => $options,
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
            ];

            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$customAttributeCode] = $customField;
        }

        return $jsLayout;
    }
}
