<?php
namespace Excellence\Customerattr\Controller\Adminhtml\Customerattr;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $customerattrfactory;
    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $_session;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var \Magento\Backend\Model\Auth
     */
    protected $_auth;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $_localeResolver;

    /**
     * @var bool
     */
    protected $_canUseBaseUrl;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;

    protected $registryFactory;

    protected $sessionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     */

    public function __construct(\Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $registryFactory,
        \Magento\Backend\Model\Session $sessionFactory,
        \Excellence\Customerattr\Model\Customerattr $customerattrfactory
    ) {
        $this->registryFactory = $registryFactory;
        $this->sessionFactory = $sessionFactory;
        $this->customerattrfactory = $customerattrfactory;
        $this->_authorization = $context->getAuthorization();
        $this->_auth = $context->getAuth();
        $this->_helper = $context->getHelper();
        $this->_backendUrl = $context->getBackendUrl();
        $this->_formKeyValidator = $context->getFormKeyValidator();
        $this->_localeResolver = $context->getLocaleResolver();
        $this->_canUseBaseUrl = $context->getCanUseBaseUrl();
        $this->_session = $context->getSession();
        parent::__construct($context);
    }

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('customerattr_id');

        $model = $this->customerattrfactory;

        $registryObject = $this->registryFactory;

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This row no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
        // 3. Set entered data if was error when we do save
        $data = $this->sessionFactory->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $registryObject->register('customerattr_customerattr', $model);
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}
