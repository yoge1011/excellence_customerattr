<?php
namespace Excellence\Customerattr\Controller\Adminhtml\Customerattr;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Controller\ResultFactory;

class Save extends \Magento\Backend\App\Action
{
    protected $customerAttrFactory;

    protected $_helper;

    protected $_session;

    protected $_authorization;

    protected $_auth;

    protected $_backendUrl;

    protected $_localeResolver;

    protected $_canUseBaseUrl;

    protected $_formKeyValidator;

    protected $sessionFactory;

    private $layoutFactory;

    protected $model;

    protected $eavConfig;

    protected $attributeSet;

    protected $customerSetupFactory;

    protected $moduleDataSetupInterface;

    protected $setFactory;

    public function __construct(\Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\Session $sessionFactory,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetupInterface,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $setFactory,
        \Excellence\Customerattr\Model\Customerattr $customerAttrFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\Product\Url $productUrl,
        \Magento\Eav\Model\Entity $eavEntity,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Eav\Model\Entity\Attribute\Set $attributeSet,
        \Magento\Framework\View\LayoutFactory $layoutFactory
    ) {
        $this->layoutFactory = $layoutFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->moduleDataSetupInterface = $moduleDataSetupInterface;
        $this->setFactory = $setFactory;
        $this->eavConfig = $eavConfig;
        $this->attributeSet = $attributeSet;
        $this->customerAttrFactory = $customerAttrFactory;
        $this->sessionFactory = $sessionFactory;
        $this->_authorization = $context->getAuthorization();
        $this->_auth = $context->getAuth();
        $this->_helper = $context->getHelper();
        $this->_backendUrl = $context->getBackendUrl();
        $this->_formKeyValidator = $context->getFormKeyValidator();
        $this->_localeResolver = $context->getLocaleResolver();
        $this->_canUseBaseUrl = $context->getCanUseBaseUrl();
        $this->_session = $context->getSession();
        parent::__construct($context, $coreRegistry, $productUrl, $eavEntity, $resultPageFactory);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if ($data) {
            $model = $this->customerAttrFactory;

            /*Creating custom attribute */
            if ($data['attribute_type'] == 'Text') {
                $input = 'text';
                $source = '';
                $type = 'text';
            } else {
                $type = 'varchar';
                $input = 'select';
                $source = 'Excellence\Customerattr\Model\Config\Source\Options';
            }

            $attributeCode = $data['attribute_code'];
            $title = $data['attribute_label'];

            $customerSetupFactory = $this->customerSetupFactory;

            $setupInterface = $this->moduleDataSetupInterface;

            $attributeSetFactory = $this->setFactory;

            $customerSetup = $customerSetupFactory->create(['setup' => $setupInterface]);

            $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();

            /** @var $attributeSet AttributeSet */
            $attributeSet = $attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, $attributeCode, [
                'type' => $type,
                'label' => $title,
                'input' => $input,
                'source' => $source,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'system' => 0,
            ]);
            //add attribute to attribute set
            $attribute = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, $attributeCode)
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_checkout', 'adminhtml_customer', 'customer_account_edit', 'checkout_register', 'customer_account_create'],
                ]);

            /**end of attribute setup */

            $model->setData($data);

            try {
                $model->save();
                $attribute->save(); //customer attribute saving
                $this->messageManager->addSuccess(__('Customer Attribute Has been Saved.'));
                $this->sessionFactory->setFormData(false);
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the attribute.' . $e->getMessage()));
            }

            $this->_redirect('*/*/edit', array('customerattr_id' => $this->getRequest()->getParam('customerattr_id')));
            return;
        }
        $this->_redirect('*/*/');
    }
}
