<?php
namespace Excellence\Customerattr\Controller\Adminhtml\Customerattr;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $customerattrfactory;

    public function __construct(Action\Context $context,
        \Excellence\Customerattr\Model\Customerattr $customerattrfactory
    ) {
        $this->customerattrfactory = $customerattrfactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $id = $this->getRequest()->getParam('customerattr_id');
        try {
            $banner = $this->_objectManager->get('Excellence\Customerattr\Model\Customerattr')->load($id);
            $banner->delete();
            $this->messageManager->addSuccess(
                __('Delete successfully !')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}
